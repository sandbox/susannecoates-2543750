(function($) {
        
            var iframe = $('#player1')[0];
            var player = $f(iframe);
            var status = $('.status');
            var duration = $('.duration');
            var position = $('.position');
            var delta = $('.delta');
            var lastPosition = 0;
            
            var umd = new google.maps.LatLng (38.986103, -76.946280);
            var umd0 = new google.maps.LatLng (38.988646, -76.935744);
            var umd1 = new google.maps.LatLng (38.988646, -76.935744);
            var umd2 = new google.maps.LatLng (38.988132, -76.938035);
            var umd3 = new google.maps.LatLng (38.987682, -76.946280);
            var umd4 = new google.maps.LatLng (38.987115, -76.940140);
            var agnr = new google.maps.LatLng(38.987190, -76.940852);
            
            var map;
            function initialize() {
                map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 14,
                center: umd,
                mapTypeId: google.maps.MapTypeId.HYBRID,
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            // When the player is ready, add listeners for pause, finish, and playProgress
            player.addEvent('ready', function() {
                status.text('Ready');
                var divid = document.getElementById("textArea1");
                divid.scrollTop = 0;
                player.addEvent('pause', onPause);
                player.addEvent('finish', onFinish);
                player.addEvent('playProgress', onPlayProgress);
            });

            // Call the API when a button is pressed
            $('button').bind('click', function() {
                player.api($(this).text().toLowerCase());
            });

            function onPause(id) {
                status.text('Paused');
            }

            function onFinish(id) {
                status.text('Finished');
                var divid = document.getElementById("textArea1");
                divid.scrollTop = 0;
            }

            function onPlayProgress(data, id) {
                status.text('Playing');
                duration.text(data.duration);
                position.text(data.seconds + ' Seconds, (' + data.percent + '%)');
                delta.text((data.seconds - lastPosition) + ' Seconds');
                lastPosition = data.seconds;
                var divid = document.getElementById("textArea1");
                divid.scrollTop += 1;
                
                if(data.seconds >= 1.0 && data.seconds <= 2.0) {
                    map.setZoom(17);
                    map.panTo(umd0);
                    document.getElementById('externalPage').src = 'http://agnr.umd.edu/news/alumni-spotlight-pete-charlerie';
                } else if(data.seconds > 3.0 && data.seconds <= 6.0) {
                    map.panTo(umd1);   
                    document.getElementById('externalPage').src = 'http://ansc.umd.edu/';
                } else if(data.seconds > 6.0 && data.seconds <= 9.0) {
                    map.panTo(umd2);     
                } else if(data.seconds > 8.0 && data.seconds <= 10.0) {
                    map.panTo(umd3);     
                    document.getElementById('externalPage').src = 'http://extension.umd.edu/poultry';
                } else if(data.seconds > 10.0 && data.seconds <= 12.0) {
                    map.panTo(umd4);     
                } else if(data.seconds > 12.0 && data.seconds <= 14.0) {
                    map.setZoom(19);
                    map.panTo(agnr);  
                    document.getElementById('externalPage').src = 'http://extension.umd.edu/news/events';
                }
                
            }
        
    
})(jQuery);